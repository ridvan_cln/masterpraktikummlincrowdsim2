# MasterPraktikumMLinCrowdSim2
### Run the python program for Task3/5
```bash
# Python 3.5 or higher required
python3 main-vadere-console.py
```

### Scenarios and Tests can be found under Scenarios folder
```bash
For Task 4: 1000 pedestrian scenario is found in: 
masterpraktikummlincrowdsim2/Scenarios/Task_4/SIR Figure 6 scenario
Fir higher infection rate:
masterpraktikummlincrowdsim2/Scenarios/Task_4/SIR Figure 6 scenario/higher-inf-rate
For corridor scenario:
masterpraktikummlincrowdsim2/Corridor_scenario
For 1000 pedestrian scenario in Task 5:
masterpraktikummlincrowdsim2/Scenarios/task5_1000_ped
For higher p and r:
masterpraktikummlincrowdsim2/Scenarios/task5_1000_ped/task5_1000_ped/Higher-p or Higher-r or Higher-p-and-r 
For the Supermarket scenario:
masterpraktikummlincrowdsim2/Scenarios/Task5_Supermarket
```


