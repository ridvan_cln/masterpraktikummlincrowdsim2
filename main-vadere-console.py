import json
import os

def add_pedestrian(d: dict, person: dict) -> None:
    """[This function will add a pedestrian to datastructure.]

    Arguments:
        d {dict} -- [The scenario file as a dictionary]
        person {dict} -- [The Person as a dictionary]
    """
    d.get('scenario').get('topography').get('dynamicElements').append(person)

def encode_obj_to_json(out: dict, output: str):
    """[This will remove an already existing output encode the dictionary to json and than create a new modified scenario.]

    Arguments:
        out {[type]} -- [dictionary of the modified datastructure]
        output {str} -- [path to the output]
    """
    if os.path.exists(output):
        os.remove(output)
    f = open(output, "x")
    f.write(json.dumps(out))
    f.close()

def decode_json_to_obj(path: str) -> dict:
    """[This function read the json file and encodes it to a dictionary.]

    Arguments:
        path {str} -- [path to the json scenario file.]

    Returns:
        [type] -- [returns a dictionary of scenario.]
    """
    with open(path, 'r') as file:
        data = file.read().replace('\n', '')
    jdata = json.loads(data)
    return jdata

def execute_vadere(path: str, output: str, outputFolder: str):
    """[This function will call vadere to run run a simulation.]

    Arguments:
        path {str} -- [path to vadere]
        output {str} -- [path to the output file of the scenario.]
        outputFolder {str} -- [path to the output file of the simulation result]
    """
    args = [path+'vadere-console.jar', 'scenario-run', '--scenario-file', output, '--output-dir='+outputFolder]
    os.system( "java -jar ./vadere-console.jar " + args[1] + " " + args[2] + " " + args[3] + " " + args[4])

def main():
    """[summary]
    """
    pers = {
        "source" : None,
        "targetIds" : [ 1 ],
        "position" : {
        "x" : 12.0,
        "y" : 2.0
        },
        "velocity" : {
        "x" : 0.0,
        "y" : 0.0
        },
        "nextTargetListIndex" : 0,
        "freeFlowSpeed" : 0.9298570083320643,
        "attributes" : {
        "id" : -1,
        "radius" : 0.2,
        "densityDependentSpeed" : False,
        "speedDistributionMean" : 1.34,
        "speedDistributionStandardDeviation" : 0.26,
        "minimumSpeed" : 0.5,
        "maximumSpeed" : 2.2,
        "acceleration" : 2.0,
        "footStepsToStore" : 4,
        "searchRadius" : 1.0,
        "angleCalculationType" : "USE_CENTER",
        "targetOrientationAngleThreshold" : 45.0
        },
        "idAsTarget" : -1,
        "isChild" : False,
        "isLikelyInjured" : False,
        "mostImportantEvent" : None,
        "salientBehavior" : "TARGET_ORIENTED",
        "groupIds" : [ ],
        "trajectory" : {
        "footSteps" : [ ]
        },
        "groupSizes" : [ ],
        "modelPedestrianMap" : None,
        "type" : "PEDESTRIAN"
    }

    decoded = decode_json_to_obj('./Scenarios/Corner_scenario_Task1/scenarios/Corner.scenario')
    add_pedestrian(decoded, pers)
    encode_obj_to_json(decoded, './Scenarios/Corner_scenario_Task1/scenarios/ModifiedCorner.scenario')
    execute_vadere('./', './Scenarios/Corner_scenario_Task1/scenarios/ModifiedCorner.scenario', './output')

if __name__ == "__main__":
    main()

